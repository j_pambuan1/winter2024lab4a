public class Student {
	private int height;
	private int age;
	private String program;
	private int amountLearnt;

	//constructor
	public Student(int height, int age){
		this.height = height;
		this.age = age;
		this.program = "";
		this.amountLearnt = 0;
	}
	
	//methods
	public void study(int amountStudied) {
		this.amountLearnt = amountLearnt + amountStudied;
	}
	
	public void sayHeight() {
		if (this.height >= 185) {
		  System.out.println("Wow, you are tall!");
		} else {
		  System.out.println("We're short! XD");
		}
	}

	public void sayProgram() {
		if (this.program.equals("Computer Science")) {
		  System.out.println("NERD!");
		} else {
		  System.out.println("Wow! " + program + " is a cool choice.");
		}
	}
	
	//getters
	
	public int getHeight(){
		return this.height;
	}
	
	public int getAge(){
		return this.age;
	}
	
	public String getProgram(){
		return this.program;
	}
	
	public int getAmountLearnt(){
		return this.amountLearnt;
	}
	
	//setters
	
	public void setHeight(int newHeight){
		this.height = newHeight;
	}
	
	public void setAge(int newAge){
		this.age = newAge;
	}
	
	public void setProgram(String newProgram){
		this.program = newProgram;
	}
	
	public void setAmountLearnt(int newAmountLearnt){
		this.amountLearnt = newAmountLearnt;
	}
}
